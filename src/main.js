import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
// import VueAMap from 'vue-amap';

import {URL,ajaxPost, getTokens, toDs, gTo, cTo, _toast, setHeight, getHeight, backT, _call, checkPhone, _confirm, isNull, _upLoadPic, _alert, replaceDetail, getSync, setSync, _upLoadVideo, removeSync, getParam, inputBlur,screenshot,showLoading,closeLoading,browseFinish,throttle,noMultipleClicks, sTo, _pay, _getLocation, testIdCard } from "./static/js/common";

Vue.use(uView);
// Vue.use(VueAMap);
// VueAMap.initAMapApiLoader({
//   key: 'd9eb6d82f55a96effad98317b9f0ce5e',
//   plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor'],
//   // 默认高德 sdk 版本为 1.4.4
//   v: '1.4.4'
// });
import amap from '@/static/js/amap-wx.130.js'
Vue.prototype.$amapPlugin = new amap.AMapWX({
    key: 'd9eb6d82f55a96effad98317b9f0ce5e',
    // key: 'cfae1941bfef1de4ca0d9dc540536b00',
    // key: '6b905456cf1516c82bb476e437d18846',
    // "plugins":["AMap.Geocoder"],
    // jscode: '596806ad6379c363376af7fbac073d8b'
});



Vue.config.productionTip = false


App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()

Vue.config.productionTip = false;
Vue.prototype.$url = URL;
Vue.prototype.$ajax = ajaxPost;
Vue.prototype.$getTokens = getTokens;
Vue.prototype.$toDs = toDs;
Vue.prototype.$gTo = gTo;
Vue.prototype.$cTo = cTo;
Vue.prototype.$toast = _toast;
Vue.prototype.$setHeight = setHeight;
Vue.prototype.$getHeight = getHeight;
Vue.prototype.$backT = backT;
Vue.prototype.$call = _call;
Vue.prototype.$checkPhone = checkPhone;
Vue.prototype.$confirm = _confirm;
Vue.prototype.$isNull = isNull;
Vue.prototype.$upLoadPic = _upLoadPic;
Vue.prototype.$alert = _alert;
Vue.prototype.$replaceDetail = replaceDetail;
Vue.prototype.$getSync = getSync;
Vue.prototype.$setSync = setSync;
Vue.prototype.$upLoadVideo = _upLoadVideo;
Vue.prototype.$removeSync = removeSync;
Vue.prototype.$getParam = getParam;
Vue.prototype.$inputBlur = inputBlur;
Vue.prototype.$screenshot = screenshot;
Vue.prototype.$showLoading = showLoading;
Vue.prototype.$closeLoading = closeLoading;
Vue.prototype.$browseFinish = browseFinish;
Vue.prototype.$throttle = throttle;
Vue.prototype.$noMultipleClicks = noMultipleClicks;
Vue.prototype.$sTo = sTo;
Vue.prototype.$pay = _pay;
Vue.prototype.$getLocation = _getLocation;
Vue.prototype.$testIdCard = testIdCard;






